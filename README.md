CASA Nowcast
=============================================================================

**Home**:
  - https://gitlab.com/perflab-exact/workflows

Source code taken from pegasus containers.

## pegasus/casa-nowcast
Container: https://hub.docker.com/r/pegasus/casa-nowcast \
Container code: https://github.com/pegasus-isi/casa-containers \
Original Workflow: https://github.com/pegasus-isi/casa-nowcast-workflow

## Dependencies
```
HDF5
NetCDF-C>=4.6
libconfig=1.5
libxml2=2.9.4
jansson=2.7
libpng=1.6.34
```
- Manually edit the `load_dep.sh` to the correct paths
- run source `load_dep.sh`, then run `make`

## Datasets
Dataset is taken from the `testdata` branch of the pegasus workflow
```
git clone https://github.com/pegasus-isi/casa-nowcast-workflow
cd casa-nowcast-workflow
git checkout testdata
```

## Run the program
- setup the correct paths, and `source load_dep.sh` to prepare environment paths
- run `make` to build each program in subdirectories

## Run the program
- setup the correct paths, and `source load_dep.sh` to prepare environment paths
- run `make` to build each program in subdirectories
### Input Datasets
- Put all input datas into `$EXPERIMENT_PATH/input`
- Input data should also include `nexrad_ref.png`.
### Time Parameters
- adjust `END_MIN` to vary the number of input files, largest input time is `529`.
- Valid `START_MIN` and `END_MIN` values:
```
START_TIME=500 # from minute 0
END_TIME=515 # to minute 15
```
### Other Parameters
- modify the below paths for use
```
EXPERIMENT_PATH="/qfs/projects/oddite/tang584/casa_nowcast_runs"
OUTPUT_PATH="$EXPERIMENT_PATH/output"
```
### Correcness
You can check output under test/example_logs for program correctness.

