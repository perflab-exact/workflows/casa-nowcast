#!/bin/bash

START_MIN=500 # earliest starttime
END_MIN=501 # latest endtime
# END_MIN=529 # latest endtime

# set -x
EXPERIMENT_PATH="/qfs/projects/oddite/tang584/casa_nowcast_runs"
INPUT_PATH="$EXPERIMENT_PATH/input"
EXP_CONF_MRT="./mrtV2_example.txt"
CONF_MRT_NAME="mrt_config.txt"


INPUT_N2P="$INPUT_PATH/nexrad_ref.png"
PROGRAM_PATH="../src"
LOG_PATH="./example_logs"
mkdir -p $LOG_PATH

OUTPUT_PATH="$EXPERIMENT_PATH/output"
mkdir -p $OUTPUT_PATH/


NOWCAST_INPUT_FILES=()
NOWCAST_OUTPUT_PATHS=()

current_min=$START_MIN
while (( current_min <= END_MIN )); do
time_seq+=("$current_min")
current_min=$(($current_min + 1))
done
echo "Analyzing Below Input Files:"
for data_time in "${time_seq[@]}"; do
    for file in "$INPUT_PATH"/*$data_time*.nc; do
        echo "$file"
        NOWCAST_INPUT_FILES+=("$file")
    done
done
echo "Total ${#NOWCAST_INPUT_FILES[@]} files"




NOWCAST () {

    rm -rf $OUTPUT_PATH/*
    ## Stage 1
    # for file in "$EXPERIMENT_PATH/input"/*.nc; do
    for file in "${NOWCAST_INPUT_FILES[@]}"; do
        filename=${file%.nc}  # Remove the .nc extension
        suffix=${filename##*_}  # Extract the last part after the last underscore
        mkdir -p $OUTPUT_PATH/$suffix
        NOWCAST_OUTPUT_PATHS+=("$OUTPUT_PATH/$suffix")
        echo "Running NowcastToWDSS2 to $OUTPUT_PATH/$suffix"
        $PROGRAM_PATH/NowcastToWDSS2/NowcastToWDSS2 $file $OUTPUT_PATH/$suffix
    done
    ls -l $OUTPUT_PATH/*/*

}

MRTV2 () {
    start_time=$(($(date +%s%N)/1000000))

    # Stage 2
    for dir in "${NOWCAST_OUTPUT_PATHS[@]}"; do
        CONFIG_MRT="${dir}/${CONF_MRT_NAME}"

        cp $EXP_CONF_MRT $CONFIG_MRT
        sed -i "s#\$OUTPUT_PATH#${dir}#" $CONFIG_MRT
        sed -i "s#\$CONF_MRT#\"${CONFIG_MRT}\"#" $CONFIG_MRT
        export MRTHOME="$CONFIG_MRT"

        echo "Running MRTV2 with $dir"
        for file in "$dir"/*.nc
        do
            # echo $file
            $PROGRAM_PATH/mrtV2/mrtV2 -c $CONFIG_MRT $file
        done
    done
    
    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed."
}

NETCDF2PNG () {
    # set -x
    start_time=$(($(date +%s%N)/1000000))

    # Stage 3

    for dir in "${NOWCAST_OUTPUT_PATHS[@]}"; do
        echo "Running NETCDF2PNG with $dir"
        for file in "$dir"/*.nc; do
            # OUTPUT_PR_FILE="PredictedReflectivity_${i}min__co-nfig00.png"

            basename=$(basename "$file" .nc)
            output_file="$dir/$basename.png"
            # echo "output_file = $output_file"

            # echo $file
            $PROGRAM_PATH/netcdf2png/merged_netcdf2png -c $INPUT_N2P \
                -q 235 \
                -z 0,75 \
                -o $output_file \
                $file

        done
    done

    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed." 
    
    ls -l $OUTPUT_PATH/* | grep png

}


# PREP_CONF
total_start_time=$(($(date +%s%N)/1000000))

NOWCAST | tee $LOG_PATH/NOWCAST.log

# Iterate over the directories in the specified path
for dir in "$OUTPUT_PATH"/*; do
    # Check if the current item is a directory
    if [ -d "$dir" ]; then
        # Add the directory path to the array
        NOWCAST_OUTPUT_PATHS+=("$dir")
    fi
done

MRTV2 | tee $LOG_PATH/MRTV2.log
NETCDF2PNG | tee $LOG_PATH/NETCDF2PNG.log

total_duration=$(( $(date +%s%N)/1000000 - $total_start_time))
echo "Total time elapsed: $total_duration milliseconds." | tee -a $LOG_PATH/NETCDF2PNG.log



