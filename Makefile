.PHONY: all

all:
	$(MAKE) -C src/NowcastToWDSS2
	$(MAKE) -C src/mrtV2
	$(MAKE) -C src/netcdf2png

clean:
	$(MAKE) -C src/NowcastToWDSS2 clean
	$(MAKE) -C src/mrtV2 clean
	$(MAKE) -C src/netcdf2png clean